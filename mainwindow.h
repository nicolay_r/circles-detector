#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMouseEvent>
#include <QEvent>
#include "hough.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    QString fileName;
    bool fileOpened;
    QSize mainWindowSize;
    QImage current_image;
    QRect image_rect;
    Hough *hough;

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    void showScaledImage(QImage& original_image);

private slots:
    void on_detectCirclesButton_clicked();
    void on_CirclesDetected();
    void on_openFile_triggered();
    void on_saveResults_triggered();

protected:
    void resizeEvent(QResizeEvent *event);
    bool eventFilter(QObject *object, QEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseMoveImageEvent(QMouseEvent* event);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
