#-------------------------------------------------
#
# Project created by QtCreator 2015-05-24T19:08:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = circles
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    hough.cpp

HEADERS  += mainwindow.h \
    hough.h

FORMS    += mainwindow.ui

OPENCV_PATH = /home/nicolay/opencv/opencv-2.4.11/           # путь к корневому каталогу opencv
LIBS_PATH += $$OPENCV_PATH/build/lib/                       # добавляем путь ко всем библиотекам opencv

LIBS += $$OPENCV_PATH/build/lib/libopencv_core.so
LIBS += $$OPENCV_PATH/build/lib/libopencv_highgui.so
LIBS += $$OPENCV_PATH/build/lib/libopencv_imgproc.so

INCLUDEPATH += $$OPENCV_PATH/modules/core/include/ \
    $$OPENCV_PATH/modules/highgui/include/
