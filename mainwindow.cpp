#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "hough.h"

#include <QFileDialog>
#include <QListView>
#include <QListWidgetItem>
#include <QString>
#include <QTextStream>

using namespace cv;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    //set MouseTracking true to capture mouse event even its key is not pressed
    this->setMouseTracking(true);

    ui->setupUi(this);

    this->mainWindowSize = this->size();
    this->fileOpened = false;
    this->hough = NULL;
}

MainWindow::~MainWindow()
{
    delete ui;
    if (hough != NULL)
        delete hough;
}

// Convert Mat to QImage
QImage Mat2QImage(const cv::Mat3b &src)
{
        QImage dest = QImage(src.cols, src.rows, QImage::Format_ARGB32);

        for (int y = 0; y < src.rows; ++y)
        {
                const cv::Vec3b *srcrow = src[y];
                QRgb *destrow = (QRgb*)dest.scanLine(y);

                for (int x = 0; x < src.cols; ++x)
                {
                        destrow[x] = qRgba(srcrow[x][2], srcrow[x][1], srcrow[x][0], 255);
                }
        }

        return dest;
}

void MainWindow::showScaledImage(QImage& original_image)
{
    // Scale for the size of a label control
    QImage scaled_image = original_image.scaled(ui->label->width(), ui->label->height(), Qt::KeepAspectRatio);

    // Save image rectangle
    image_rect = QRect(
                ui->label->geometry().x(),
                ui->label->geometry().y() + this->ui->mainToolBar->geometry().height() + this->ui->menuBar->geometry().height(),
                scaled_image.width(),
                scaled_image.height());

    // Render scaled image
    ui->label->setAlignment(Qt::AlignLeft | Qt::AlignTop );
    ui->label->setPixmap(QPixmap::fromImage(scaled_image, Qt::AutoColor));
}

void MainWindow::resizeEvent(QResizeEvent* event)
{
    QMainWindow::resizeEvent(event);

    // calculate delta
    int dw = width() - this->mainWindowSize.width();
    int dh = height() - this->mainWindowSize.height();

    if (ui->label->width() + dw > 0
            && ui->label->height() + dh > 0)
    {
        // resize label
        ui->label->setGeometry(
                ui->label->pos().x(),
                ui->label->pos().y(),
                ui->label->width() + dw,
                ui->label->height() + dh);

        // show image
        if (fileOpened)
            showScaledImage(current_image);

        // set new size
        mainWindowSize = this->size();
    }
}

void MainWindow::mouseMoveImageEvent(QMouseEvent *event)
{
    this->ui->imageCoordsLabel->setText(
                QString("Координаты изображения: %1, %2")
                    .arg(
                        QString::number((event->x() - image_rect.x())*current_image.width()/image_rect.width()),
                        QString::number((event->y() - image_rect.y())*current_image.height()/image_rect.height())
                    )
                );
}

bool MainWindow::eventFilter(QObject *, QEvent *event)
{
    if (event->type() == QEvent::MouseMove)
    {
        QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);

        // Check that cursor in image_rect
        if (image_rect.contains(mouseEvent->x(), mouseEvent->y()))
        {
            this->mouseMoveImageEvent(mouseEvent);
        }

        return true;
    }

    return QWidget::event(event);
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    this->ui->imageCoordsLabel->setText(QString("Координаты курсора: %1, %2")
                                        .arg(QString::number(event->x()),
                                             QString::number(event->y())
                                        ));
    eventFilter(this, event);
}



void MainWindow::on_detectCirclesButton_clicked()
{
    if (!fileOpened)
        return;

    // Process Image
    hough->findCircles(
                this->ui->densityPixels->text().toDouble(0),
                this->ui->minDistBetweenCircles->text().toDouble(0),
                this->ui->edgeThreshold->text().toDouble(0),
                this->ui->centerThreshold->text().toDouble(0));

    current_image = Mat2QImage(hough->ProcessedImage());

    on_CirclesDetected();

    showScaledImage(current_image);

    // Show List of Circles
    QListWidget *listWidget = new QListWidget(0);

    if (hough != NULL)
    {
        vector<Vec3f> circles = *(hough->Circles());

        for (unsigned int i = 0; i < circles.size(); i++)
        {
            listWidget->addItem(QString("%1, X: %2, Y: %3, Радиус: %4")
                                .arg(QString::number(i+1),
                                     QString::number(cvRound(circles[i][0])),
                                     QString::number(cvRound(circles[i][1])),
                                     QString::number(cvRound(circles[i][2]))
                                ));
        }
    }

    listWidget->show();
}

void MainWindow::on_CirclesDetected()
{
    QList<QAction*> actions = this->ui->menuCircles->actions();

    for (int i = 0; i < actions.size(); i++)
    {
        QAction* a = actions.at(i);

        if (a->objectName() == "saveResults")
        {
            // enable button
            a->setEnabled(true);
            break;
        }
    }
}

void MainWindow::on_openFile_triggered()
{
    this->fileName = QFileDialog::getOpenFileName(this, "browse", "/", "*.jpg", 0, 0);

    if (fileName != NULL)
    {
        // File opened
        fileOpened = true;

        // Create Mat
        Mat img = imread(this->fileName.toStdString(), 1);
        hough = new Hough(img);
        current_image = Mat2QImage(img);

        // Show scaled image
        showScaledImage(current_image);
    }
}

void MainWindow::on_saveResults_triggered()
{
    QString filepath = QFileDialog::getSaveFileName(this, "Сохранить", "/", QString(), 0, 0);

    // Create file and save
    QFile file(filepath);
    if (file.open(QIODevice::ReadWrite))
    {
        QTextStream stream(&file);

        vector<Vec3f> circles = *(hough->Circles());
        for (unsigned int i = 0; i < circles.size(); i++)
        {
            stream << QString("%1, X: %2, Y: %3, Радиус: %4")
                                .arg(QString::number(i+1),
                                     QString::number(cvRound(circles[i][0])),
                                     QString::number(cvRound(circles[i][1])),
                                     QString::number(cvRound(circles[i][2]))
                                ) << endl;
        }

    }


}
