#include "hough.h"
#include <opencv2/core/core.hpp>    /* cv:: */
#include <QColor>                   /* QRgb */
#include <QImage>
#include <string>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;

Hough::Hough(Mat &img)
{
    this->img = img;
}

Mat Hough::ProcessedImage()
{
    return this->proc_img;
}

vector<Vec3f>* Hough::Circles()
{
    return &circles;
}

void Hough::findCircles(double dp, double minDistBetweenCircles, double edgeThreshold, double centerThreshold)
{
    // take a copy of image
    img.copyTo(proc_img);

    // convert image to gray
    Mat gray;
    cvtColor(proc_img, gray, CV_BGR2GRAY);
    GaussianBlur(gray, gray, Size(9,9), 2, 2);

    // Find Circles
    HoughCircles(gray, circles, CV_HOUGH_GRADIENT, dp, minDistBetweenCircles, edgeThreshold, centerThreshold, 0, 0);

    // Draw Circles
    for (unsigned int i = 0; i < circles.size(); i++)
    {
        Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
        int radius = cvRound(circles[i][2]);

        // draw center
        circle(proc_img, center, 2, Scalar(255, 0, 0), 4, 8, 0);

        // draw circle
        circle(proc_img, center, radius, Scalar(0, 0, 255), 8, 8, 0);

        // draw number of circle
        putText(proc_img, QString::number(i+1).toStdString(), center, FONT_HERSHEY_COMPLEX, 1, Scalar(0, 255, 0), 2, 8, false);
    }
}




