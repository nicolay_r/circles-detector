#ifndef HOUGH_H
#define HOUGH_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <QApplication>
#include <vector>

using namespace std;
using namespace cv;

class Hough
{
private:
    Mat img;
    Mat proc_img;                 // обработанное изображение
    vector<Vec3f> circles;
public:
    Mat ProcessedImage();
    vector<Vec3f>* Circles();
    void findCircles(double dp, double minDistBetweenCircles, double edgeThreshold, double centerThreshold);
    Hough(Mat& img);
};

#endif // HOUGH_H
